// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
    
#include <iostream>
#include <fstream>
#include <map>
#include <chrono>

#include <boost/property_map/property_map.hpp>

#include <Eigen/Dense>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Surface_mesh.h>
#include <CGAL/Polygon_mesh_processing/connected_components.h>
#include <CGAL/boost/graph/Face_filtered_graph.h>
#include <CGAL/Polygon_mesh_processing/measure.h>
#include <CGAL/Polygon_mesh_processing/compute_normal.h>
#include <CGAL/AABB_tree.h>
#include <CGAL/AABB_traits.h>
#include <CGAL/AABB_face_graph_triangle_primitive.h>
#include <CGAL/Polygon_mesh_processing/orientation.h>
#include <CGAL/Polygon_mesh_processing/triangulate_faces.h>

#include <CLI/CLI.hpp>

using real_t = float;

using Kernel = CGAL::Simple_cartesian<real_t>;
using Point = Kernel::Point_3;
using Mesh = CGAL::Surface_mesh<Point>;
using face_descriptor = boost::graph_traits<Mesh>::face_descriptor;
using faces_size_type = boost::graph_traits<Mesh>::faces_size_type;
using FCCmap = Mesh::Property_map<face_descriptor, faces_size_type>;
using Filtered_graph = CGAL::Face_filtered_graph<Mesh>;
using Primitive = CGAL::AABB_face_graph_triangle_primitive<Mesh>;
using Traits = CGAL::AABB_traits<Kernel, Primitive>;
using Tree = CGAL::AABB_tree<Traits>;

namespace PMP = CGAL::Polygon_mesh_processing;

using Vec = Eigen::Matrix < real_t, 1, -1 >;
using Mat = Eigen::Matrix < real_t, 3, -1 >;
using Vec3 = Eigen::Matrix<real_t, 3, 1>;

struct Timer {
  using Clock = std::chrono::system_clock;

  void
  Start () {
    m_start = Clock::now ();
  }

  void
  End () {
    m_end = Clock::now ();
    m_time = m_end - m_start;
  }

  double ElapsedTime () {
    return m_time.count ();
  }

 private:
  std::chrono::time_point<Clock> m_start;
  std::chrono::time_point<Clock> m_end;
  std::chrono::duration<double> m_time;
};

int main ( int argc, char *argv[] ) {
  CLI::App app{"Split the mesh in connected components and compute various stats"};

  std::string in_file, in_file_pores, out_file_pores = "", out_file_stat = "";
  real_t ratio_z = 0., min_volume = std::nan ( "1" ), voxel_size = 0., min_voxel = 8.;

  app.add_option ( "-i,--input_mesh", in_file, "Input mesh" )->required ();
  app.add_option ( "--ip,--input_pores", in_file_pores, "Input pores mesh" )->required ();
  app.add_option ( "--vs, --voxel_size", voxel_size, "Voxel size" )->default_val ( std::to_string ( voxel_size ) );
  app.add_option ( "--min_voxel", min_voxel, "Minimul number of voxels than can have a pore" )->default_val ( std::to_string ( min_voxel ) );
  app.add_option ( "--min_volume", min_volume, "Minimul volume than can have a pore, usually equal to 8 * voxel_volme of the CT data. If set the voxel_size and min_voxel are ignored" )->default_val ( std::to_string ( min_volume ) );
  app.add_option ( "--rz,--ratio_z", ratio_z, "The ratio, from the top and the bottom of the input mesh, that has to be removed to compute the pores" )->default_val ( std::to_string ( ratio_z ) );
  app.add_option ( "--op,--output_pores", out_file_pores, "Output pores mesh" )->default_val ( out_file_pores );
  app.add_option ( "--os,--output_stat", out_file_stat, "Output file with the pores statistics" )->default_val ( out_file_stat );

  CLI11_PARSE ( app, argc, argv );

  if ( out_file_pores.empty () && out_file_stat.empty () ) {
    std::cerr << "The output pores mesh or the output statitistics must not be empty\n";
    return EXIT_FAILURE;
  }

  if ( std::isnan ( min_volume ) ) {
    min_volume = min_voxel * std::pow ( voxel_size, 3 );
  }

  Timer timer;
  std::cout << "Read meshes: " << std::flush;
  timer.Start ();

  // Read meshes
  Mesh mesh, mesh_pores;

  std::ifstream in_file_s ( in_file.c_str () );
  if ( !in_file_s || ! ( CGAL::read_ply ( in_file_s, mesh ) ) || mesh.is_empty() ) {
    std::cerr << in_file << " is not a valid ply file" << std::endl;
    return 1;
  }
  in_file_s.close ();

  in_file_s.open ( in_file_pores.c_str () );
  if ( !in_file_s || ! ( CGAL::read_ply ( in_file_s, mesh_pores ) ) || mesh_pores.is_empty() ) {
    std::cerr << in_file_pores << " is not a valid ply file" << std::endl;
    return 1;
  }
  in_file_s.close ();

  // Triangulate pores mesh
  PMP::triangulate_faces ( mesh_pores );

  timer.End ();
  std::cout << timer.ElapsedTime () << " s\n";

  std::cout << "Splitting pores: " << std::flush;
  timer.Start ();

  // Compute connected components and keep the pores
  FCCmap fccmap_pores = mesh_pores.add_property_map<face_descriptor, faces_size_type> ( "f:CC" ).first;
  faces_size_type n_mesh_pores = PMP::connected_components ( mesh_pores, fccmap_pores );

  timer.End ();
  std::cout << timer.ElapsedTime () << " s\n";
  std::cout << "Total number of components: " << n_mesh_pores << "\n";

  std::cout << "Compute statistics: " << std::flush;
  timer.Start ();

  // Compute per pore center of mass, area and volume
  std::vector<Vec3> mesh_pores_cm ( n_mesh_pores, Vec3::Zero () );
  std::vector<real_t> mesh_pores_area ( n_mesh_pores, 0. );
  std::vector<real_t> mesh_pores_volume ( n_mesh_pores, 0. );

  const auto &points = mesh_pores.points ();

  for ( auto &&face : mesh_pores.faces () ) {
    // auto &cm = face_cm[face.idx ()];
    Vec3 cm = Vec3::Zero ();
    std::vector<Vec3> v_tmp ( 3, Vec3::Zero () );

    int i = 0;
    for ( auto &&v : mesh_pores.vertices_around_face ( mesh_pores.halfedge ( face ) ) ) {
      const auto &tmp = points[v];
      v_tmp[i][0] += tmp[0];
      v_tmp[i][1] += tmp[1];
      v_tmp[i][2] += tmp[2];

      cm += v_tmp[i];

      ++i;
    }

    // Face center of mass
    cm /= 3.;

    // Face area
    const auto &idx = face.idx ();
    const auto area_tmp = ( v_tmp[1] - v_tmp[0] ).cross ( v_tmp[2] - v_tmp[0] ).norm () / 2.;

    // Pore area
    const auto &idx_pore = fccmap_pores[face];
    mesh_pores_area[idx_pore] += area_tmp;

    // Pore center of mass
    mesh_pores_cm[idx_pore] += area_tmp * cm;

    // Pore volume
    const auto v210 = v_tmp[2][0] * v_tmp[1][1] * v_tmp[0][2];
    const auto v120 = v_tmp[1][0] * v_tmp[2][1] * v_tmp[0][2];
    const auto v201 = v_tmp[2][0] * v_tmp[0][1] * v_tmp[1][2];
    const auto v021 = v_tmp[0][0] * v_tmp[2][1] * v_tmp[1][2];
    const auto v102 = v_tmp[1][0] * v_tmp[0][1] * v_tmp[2][2];
    const auto v012 = v_tmp[0][0] * v_tmp[1][1] * v_tmp[2][2];

    mesh_pores_volume[idx_pore] += std::abs ( - v210 + v120 + v201 - v021 - v102 + v012 );
  }

  for ( size_t i = 0; i < n_mesh_pores; ++i ) {
    mesh_pores_cm[i] /= mesh_pores_area[i];
    mesh_pores_volume[i] /= 6.;
  }

  timer.End ();
  std::cout << timer.ElapsedTime () << " s\n";

  std::cout << "\nRemove small pores: " << std::flush;
  timer.Start ();

  std::vector<size_t> idx_remove;
  std::array<size_t, 2> n_removed = { 0, 0 };
  if ( min_volume > std::numeric_limits<real_t>::epsilon () ) {
    for ( size_t i = 0; i < n_mesh_pores; ++i ) {
      if ( mesh_pores_volume[i] < min_volume ) {
        idx_remove.push_back ( i );
      }
    }

    n_removed[0] = n_removed[1] = idx_remove.size ();
    n_mesh_pores -= n_removed[0];
  }

  timer.End ();
  std::cout << timer.ElapsedTime () << " s\n";
  std::cout << "Minimum volume " << min_volume << "\n";
  std::cout << "Removed " << n_removed[0] << " pores\n";

  std::cout << "\nRemove out of range pores: " << std::flush;
  timer.Start ();

  // Compute center of mass pores with z mesh range
  if ( ratio_z > std::numeric_limits<real_t>::epsilon () ) {
    const auto inf = std::numeric_limits<real_t>::infinity();
    Eigen::Matrix<real_t, 1, 2> range_z { inf, -inf };

    const auto mesh_points = mesh.points();
    for ( auto &v : mesh.vertices () ) {
      const auto &z = mesh_points[v].z();

      if ( z < range_z[0] ) {
        range_z[0] = z;
        continue;
      }

      if ( z > range_z[1] ) {
        range_z[1] = z;
      }
    }

    real_t step_z = ( range_z[1] - range_z[0] ) * ratio_z;
    range_z[0] += step_z;
    range_z[1] -= step_z;

    // Check index to remove
    auto idx_remove_it = idx_remove.cbegin ();

    for ( size_t i = 0; i < mesh_pores_cm.size (); ++i ) {
      if ( i == *idx_remove_it ) {
        ++idx_remove_it;
        continue;
      }

      const auto &z = mesh_pores_cm[i][2];
      if ( z < range_z[0] || z > range_z[1] ) {
        idx_remove.push_back ( i );
      }
    }

    // Remove pores
    n_removed[0] = idx_remove.size () - n_removed[1];
    if ( n_removed[0] > 0 ) {
      std::sort ( idx_remove.begin (), idx_remove.end () );
      n_mesh_pores -= n_removed[0];
      n_removed[1] = idx_remove.size ();
    }
  }

  timer.End ();
  std::cout << timer.ElapsedTime () << " s\n";
  std::cout << "Removed " << n_removed[0] << " pores\n";

  // Check if the center of mass are inside the mesh
  std::cout << "\nRemove pores outside the mesh and compute sphericity: " << std::flush;
  timer.Start ();

  std::vector<real_t> sign_dist ( mesh_pores_area.size (), 0. );
  {
    // Compute mesh normal
    auto fnormals = mesh.add_property_map<face_descriptor, Kernel::Vector_3> ( "f:normals", CGAL::NULL_VECTOR ).first;
    PMP::compute_face_normals ( mesh, fnormals );

    // Check orientation
    const real_t k_0 = PMP::is_outward_oriented ( mesh ) ? 1. : -1.;

    // Construct tree
    Tree tree ( faces ( mesh ).first, faces ( mesh ).second, mesh );
    tree.accelerate_distance_queries ();

    auto idx_remove_it = idx_remove.cbegin ();
    for ( size_t i = 0; i < mesh_pores_cm.size (); ++i ) {
      if ( i == *idx_remove_it ) {
        ++idx_remove_it;
        continue;
      }

      // Compute closest point
      const auto &pt = mesh_pores_cm[i];
      const auto pt_tmp = Point ( pt[0], pt[1], pt[2] );

      const auto [pt_proj_tmp, face] = tree.closest_point_and_primitive ( pt_tmp );

      const Vec3 pt_proj = { pt_proj_tmp[0], pt_proj_tmp[1], pt_proj_tmp[2] };
      const auto &fnormal_tmp = fnormals[face];
      const Vec3 fnormal = { fnormal_tmp[0], fnormal_tmp[1], fnormal_tmp[2] };

      // Compute signed distance (it is assumed that the mesh is outward oriented)
      const Vec3 dist = pt - pt_proj;
      const real_t k = k_0 * ( dist.dot ( fnormal ) > 0. ? 1. : -1. );
      const auto sign_dist_value = k * dist.norm ();

      if ( sign_dist_value > std::numeric_limits<real_t>::epsilon () ) {
        idx_remove.push_back ( i );
      } else {
        sign_dist[i] = sign_dist_value;
      }
    }

    n_removed[0] = idx_remove.size () - n_removed[1];
    if ( n_removed[0] > 0 ) {
      std::sort ( idx_remove.begin (), idx_remove.end () );
      n_mesh_pores -= n_removed[0];
      n_removed[1] = idx_remove.size ();
    }
  }

  // Not possible to do before because the index of the connected components is not updated
  if ( idx_remove.size () > 0 ) {
    PMP::remove_connected_components ( mesh_pores, idx_remove, fccmap_pores );

    for ( int i = idx_remove.size () - 1; i > -1; --i ) {
      const auto &idx = idx_remove[i];
      mesh_pores_cm.erase ( mesh_pores_cm.begin() + idx );
      mesh_pores_area.erase ( mesh_pores_area.begin() + idx );
      mesh_pores_volume.erase ( mesh_pores_volume.begin() + idx );
      sign_dist.erase ( sign_dist.begin () + idx );
    }
  }

  // Compute sphericity
  std::vector<real_t> mesh_pores_sph ( n_mesh_pores, 0. );

  for ( size_t i = 0; i < n_mesh_pores; ++i ) {
    mesh_pores_sph[i] = std::pow ( 36. * M_PI * std::pow ( mesh_pores_volume[i], 2 ) / std::pow ( mesh_pores_area[i], 3 ), 1. / 3. );
  }

  timer.End ();
  std::cout << timer.ElapsedTime () << " s\n";
  std::cout << "Removed " << n_removed[0] << " pores\n";
  std::cout << "Total number of pores: " << n_mesh_pores << "\n";

  // Save mesh and statistics
  std::cout << "\nSave mesh and stats: " << std::flush;
  timer.Start ();

  if ( out_file_pores.empty () == false ) {
    std::ofstream out_name ( out_file_pores.c_str () );
    CGAL::set_binary_mode ( out_name );
    CGAL::write_ply ( out_name, mesh_pores );
    out_name.close ();
  }

  if ( out_file_stat.empty () == false ) {
    std::ofstream out_name ( out_file_stat.c_str () );
    out_name << "x_cm y_cm z_cm cm_sign_dist area volume sphericity\n";

    for ( size_t i = 0; i < n_mesh_pores; ++i ) {
      out_name << mesh_pores_cm[i].transpose () << " " << sign_dist[i] << " " << mesh_pores_area[i] << " " << mesh_pores_volume[i] << " " << mesh_pores_sph[i] << "\n";
    }

    out_name.close ();
  }

  timer.End ();
  std::cout << timer.ElapsedTime () << " s\n";

  return 0;
}
