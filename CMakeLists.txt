cmake_minimum_required(VERSION 3.10)
project(pore_analysis)

set(CMAKE_CONFIGURATION_TYPES "Debug;RelWithDebInfo;Release" CACHE STRING "possible configurations" FORCE)

# In case the user does not setup CMAKE_BUILD_TYPE, assume it's Release
if("${CMAKE_BUILD_TYPE}" STREQUAL "")
set(CMAKE_BUILD_TYPE Release CACHE STRING "build type default to Release" FORCE)
endif("${CMAKE_BUILD_TYPE}" STREQUAL "")

if (CMAKE_BUILD_TYPE STREQUAL "Release")
  set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DEIGEN_NO_DEBUG")
endif ()
if (CMAKE_BUILD_TYPE STREQUAL "RelWithDebInfo")
  set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DEIGEN_NO_DEBUG")
endif ()

# Set C++ 17
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

# Eigen
find_package (Eigen3 3.3 REQUIRED NO_MODULE)

# CGAL 
find_package(CGAL REQUIRED)

# OpenMP
find_package(OpenMP REQUIRED)

add_executable(${PROJECT_NAME} main.cpp)
target_link_libraries(${PROJECT_NAME} CGAL::CGAL Eigen3::Eigen OpenMP::OpenMP_CXX)

install(TARGETS ${PROJECT_NAME} RUNTIME DESTINATION bin)
